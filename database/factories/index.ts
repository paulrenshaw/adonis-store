import Factory from '@ioc:Adonis/Lucid/Factory'
import Order from 'App/Models/Order'
import Product from 'App/Models/Product'
import Transaction from 'App/Models/Transaction'
import User from 'App/Models/User'
import { ProductCategory } from '../../types/products'

export const ProductFactory = Factory.define(Product, async ({ faker }) => {
    return {
        title: faker.fake("{{commerce.productAdjective}} {{commerce.product}}"),
        price: Number(faker.finance.amount(0.00, 9.99, 2)),
        category: faker.random.arrayElement(Object.values(ProductCategory))
    }
})
.relation('transaction', () => TransactionFactory)
.build()

export const TransactionFactory = Factory.define(Transaction, async () => {
    return {
        quantity: Math.ceil(Math.random() * 10)
    }
})
.relation('order', () => OrderFactory)
.relation('product', () => ProductFactory)
.build()

export const OrderFactory = Factory.define(Order, async () => {
    return {}
})
.relation('transactions', () => TransactionFactory)
.relation('user', () => UserFactory)
.build()

export const UserFactory = Factory.define(User, async ({ faker }) => {
    return {
        username: faker.internet.userName(),
        password: faker.internet.password()
    }
})
.relation('orders', () => OrderFactory)
.build()
