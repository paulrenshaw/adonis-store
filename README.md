# Adonis Store

A simple store application built with AdonisJs 5 https://preview.adonisjs.com

## Initial Setup

- Install dependencies
    ```
    npm install
    ```

- Copy/Rename `.env.example` to `.env` (example values are included for a typical development environment)

- Generate a new APP_KEY and copy into `.env`
    ```
    node ace generate:key
    ```

## Database Setup

The example environment variables are configured to use MySQL (npm dependency is also included in `package.json`).

**Local MySQL Installation**

You will need to install MySQL on your local machine and configure a new database to match the environment variables.

**Using Docker**

Alternatively, you can use docker for the database. A `docker-compose.yml` is included along with another `.env.example` file to copy in the `docker/database` directory. Again, this is configured for MySQL.

With docker running on your local machine, simply run 
```
docker-compose up
```
in the terminal in the project directory.

*Installing Docker Desktop is the simplest solution for this. You will need the Pro version of WIndows if you are working on a windows machine.* https://www.docker.com/products/docker-desktop

### Migrations and Seeders

To create the necessary tables in the database and some initial data, you will first need to compile the typescript source code. To do this, simply run 
```
npm run build
```

You could also run 
```
npm run serve:dev
```
which will build and serve the app in watch mode to automatically rebuild whenever you make any changes.

- Run Database Migrations
    
    `node ace migration:run`

- Seed with initial User
    
    `node ace db:seed 'database/seeders/User'`

- Seed with an Order containing some Products and Transactions (aka line items) linked to the first User
    
    `node ace db:seed 'database/seeders/Order'`

    *You can run this mutiple times to create additional Orders*

