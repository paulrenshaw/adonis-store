import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Transactions extends BaseSchema {
  protected tableName = 'transactions'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id').unsigned().references('id').inTable('products')
      table.integer('order_id').unsigned().references('id').inTable('orders')
      table.integer('quantity').unsigned().defaultTo(0)
      table.decimal('item_price', 4, 2).defaultTo(0.00)
      table.decimal('subtotal', 6, 2).defaultTo(0.00)
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
