import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'
import { OrderFactory, ProductFactory, TransactionFactory } from 'Database/factories'

export default class OrderSeeder extends BaseSeeder {
  public async run() {

    const user = await (await User.firstOrNew({ id: 1 })).save()

    const order = await OrderFactory.merge({
      userId: user.id
    }).create()

    const products = await ProductFactory.createMany(Math.ceil(Math.random() * 5))

    const transactions = await Promise.all(products.map(async product => {
      try {
        const quantity = Math.ceil(Math.random() * 5)
        return await TransactionFactory.merge({
          orderId: order.id,
          productId: product.id,
          quantity,
          itemPrice: product.price,
          subtotal: product.price * quantity
        }).create()
      } catch (err) {
        console.log(err)
      }
    }))

    const total = transactions.reduce((sum, t) => {
      return sum + t!.subtotal
    }, 0)

    await order.merge({ total }).save()

  }
}
